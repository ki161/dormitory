-- ----------------------------------------------------------------------------
-- MySQL Workbench Migration
-- Migrated Schemata: dormitory
-- Source Schemata: dormitory
-- Created: Mon Jul  8 21:36:06 2019
-- Workbench Version: 8.0.16
-- ----------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------------------------------------------------------
-- Schema dormitory
-- ----------------------------------------------------------------------------
DROP SCHEMA IF EXISTS `dormitory` ;
CREATE SCHEMA IF NOT EXISTS `dormitory` ;

-- ----------------------------------------------------------------------------
-- Table dormitory.app_files
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`app_files` (
  `id` INT(11) NOT NULL,
  `application_id` INT(11) NOT NULL,
  `filepath` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `filepath` (`filepath` ASC) VISIBLE,
  INDEX `application_id` (`application_id` ASC) VISIBLE,
  CONSTRAINT `app_files_ibfk_1`
    FOREIGN KEY (`application_id`)
    REFERENCES `dormitory`.`applications` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.app_privileges
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`app_privileges` (
  `id` INT(11) NOT NULL,
  `application_id` INT(11) NOT NULL,
  `privilege_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `application_id` (`application_id` ASC) VISIBLE,
  INDEX `privilege_id` (`privilege_id` ASC) VISIBLE,
  CONSTRAINT `app_privileges_ibfk_1`
    FOREIGN KEY (`application_id`)
    REFERENCES `dormitory`.`applications` (`id`),
  CONSTRAINT `app_privileges_ibfk_2`
    FOREIGN KEY (`privilege_id`)
    REFERENCES `dormitory`.`privileges` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.applications
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`applications` (
  `id` INT(11) NOT NULL,
  `full_name` VARCHAR(255) NOT NULL,
  `pass_data` VARCHAR(255) NULL DEFAULT NULL,
  `faculty_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `pass_data` (`pass_data` ASC) VISIBLE,
  INDEX `faculty_id` (`faculty_id` ASC) VISIBLE,
  CONSTRAINT `applications_ibfk_1`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `dormitory`.`faculties` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.dormitories
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`dormitories` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC) VISIBLE,
  CONSTRAINT `dormitories_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `dormitory`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.faculties
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`faculties` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id` (`user_id` ASC) VISIBLE,
  CONSTRAINT `faculties_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `dormitory`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.places
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`places` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `room_id` INT(11) NULL DEFAULT NULL,
  `settler_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `room_id` (`room_id` ASC) VISIBLE,
  INDEX `settler_id` (`settler_id` ASC) VISIBLE,
  CONSTRAINT `places_ibfk_1`
    FOREIGN KEY (`room_id`)
    REFERENCES `dormitory`.`rooms` (`id`),
  CONSTRAINT `places_ibfk_2`
    FOREIGN KEY (`settler_id`)
    REFERENCES `dormitory`.`settlers` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.privileges
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`privileges` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `priority` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.rooms
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`rooms` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `dormitory_id` INT(11) NULL DEFAULT NULL,
  `faculty_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `dormitory_id` (`dormitory_id` ASC) VISIBLE,
  INDEX `faculty_id` (`faculty_id` ASC) VISIBLE,
  CONSTRAINT `rooms_ibfk_1`
    FOREIGN KEY (`dormitory_id`)
    REFERENCES `dormitory`.`dormitories` (`id`),
  CONSTRAINT `rooms_ibfk_2`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `dormitory`.`faculties` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.settlement_history
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`settlement_history` (
  `id` INT(11) NOT NULL,
  `settler_id` INT(11) NOT NULL,
  `place_id` INT(11) NOT NULL,
  `start_date` DATE NULL DEFAULT NULL,
  `end_date` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `settler_id` (`settler_id` ASC) VISIBLE,
  INDEX `place_id` (`place_id` ASC) VISIBLE,
  CONSTRAINT `settlement_history_ibfk_1`
    FOREIGN KEY (`settler_id`)
    REFERENCES `dormitory`.`settlers` (`id`),
  CONSTRAINT `settlement_history_ibfk_2`
    FOREIGN KEY (`place_id`)
    REFERENCES `dormitory`.`places` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.settlers
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`settlers` (
  `id` INT(11) NOT NULL,
  `full_name` VARCHAR(255) NOT NULL,
  `pass_data` VARCHAR(255) NULL DEFAULT NULL,
  `faculty_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `pass_data` (`pass_data` ASC) VISIBLE,
  INDEX `faculty_id` (`faculty_id` ASC) VISIBLE,
  CONSTRAINT `settlers_ibfk_1`
    FOREIGN KEY (`faculty_id`)
    REFERENCES `dormitory`.`faculties` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.user_roles
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`user_roles` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name` (`name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

-- ----------------------------------------------------------------------------
-- Table dormitory.users
-- ----------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `dormitory`.`users` (
  `id` INT(11) NOT NULL,
  `full_name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `login` VARCHAR(255) NULL DEFAULT NULL,
  `password` VARCHAR(255) NOT NULL,
  `roleid` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email` (`email` ASC) VISIBLE,
  UNIQUE INDEX `login` (`login` ASC) VISIBLE,
  INDEX `roleid` (`roleid` ASC) VISIBLE,
  CONSTRAINT `users_ibfk_1`
    FOREIGN KEY (`roleid`)
    REFERENCES `dormitory`.`user_roles` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;
SET FOREIGN_KEY_CHECKS = 1;
