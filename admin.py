from django.contrib import admin
from MyProjektDJ.models import User_roles, Users, Dormitories, Faculties, Rooms, Settlers, Places, Applications, Privileges, App_files, Settlement_history, App_privileges

admin.site.register(User_roles)
admin.site.register(Users)
admin.site.register(Dormitories)
admin.site.register(Faculties)
admin.site.register(Rooms)
admin.site.register(Settlers)
admin.site.register(Places)
admin.site.register(Applications)
admin.site.register(Privileges)
admin.site.register(App_files)
admin.site.register(Settlement_history)
admin.site.register(App_privileges)