from django.db import models

class User_roles(models.Model):
	name = models.CharField(max_length = 200)

class Users(models.Model):
	full_name = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	login = models.CharField(max_length=200)
	password = models.CharField(max_length=200)
	roleid = models.ForeignKey(User_roles, on_delete=models.CASCADE)

class Dormitories(models.Model):
	name = models.CharField(max_length=200)
	address = models.CharField(max_length=200)
	user_id = models.ForeignKey(Users, on_delete=models.CASCADE)

class Faculties(models.Model):
	name = models.CharField(max_length = 200)
	user_id = models.ForeignKey(Users, on_delete=models.CASCADE)

class Rooms(models.Model):
	name = models.CharField(max_length=200)
	dormitory_id = models.ForeignKey(Dormitories, on_delete=models.CASCADE)
	faculty_id = models.ForeignKey(Faculties, on_delete=models.CASCADE)
	
class Settlers(models.Model):
	full_name = models.CharField(max_length=200)
	pass_data =  models.CharField(max_length=200)
	faculty_id = models.ForeignKey(Faculties, on_delete=models.CASCADE)

class Places(models.Model):
	name = models.CharField(max_length=200)
	room_id = models.ForeignKey(Rooms, on_delete=models.CASCADE)
	settler_id = models.ForeignKey(Settlers, on_delete=models.CASCADE)

class Applications(models.Model):
	full_name = models.CharField(max_length=200)
	pass_data =  models.TextField()
	faculty_id = models.ForeignKey(Faculties, on_delete=models.CASCADE)

class Privileges(models.Model):
	name = models.CharField(max_length = 200)
	priority = models.CharField(max_length = 200)

class App_files(models.Model):
	application_id = models.ForeignKey(Applications, on_delete=models.CASCADE)
	filepath = models.CharField(max_length = 200)

class Settlement_history(models.Model):
	settler_id = models.ForeignKey(Settlers, on_delete=models.CASCADE)
	place_id = models.ForeignKey(Places, on_delete=models.CASCADE)
	start_date = models.DateField(auto_now=False, auto_now_add=True)
	end_date = models.DateField(auto_now=False, auto_now_add=True)

class App_privileges(models.Model):
	application_id = models.ForeignKey(Applications, on_delete=models.CASCADE)
	privilege_id = models.ForeignKey(Privileges, on_delete=models.CASCADE)	