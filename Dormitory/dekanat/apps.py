from django.apps import AppConfig


class DekanatConfig(AppConfig):
    name = 'dekanat'
