from django.apps import AppConfig


class DormmanagerConfig(AppConfig):
    name = 'dormmanager'
